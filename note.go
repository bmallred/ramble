package ramble

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"strings"
)

type Keyword struct {
	Label string `json:"label"`
	Text  string `json:"text"`
}

type Note struct {
	Id       string    `json:"id"`
	Path     string    `json:"path"`
	Keywords []Keyword `json:"keywords"`
	Links    []string  `json:"links"`
	Checksum string    `json:"checksum"`
}

// IsDirty returns a boolean value indicating if a change has been made to a note.
func (note *Note) IsDirty() bool {
	if note.Path == "" {
		return false
	}

	contents, err := ioutil.ReadFile(note.Path)
	if err != nil {
		return false
	}

	reader := bytes.NewReader(contents)
	checksum, err := Checksum(reader)
	if err != nil {
		return false
	}

	if note.Checksum == checksum {
		return false
	}
	return true
}

// AddKeywords will only add unique keywords to the note.
func (note *Note) AddKeywords(keywords ...Keyword) {
	for _, keyword := range keywords {
		if !contains(note.Keywords, keyword) {
			note.Keywords = append(note.Keywords, keyword)
		}
	}
}

// AddLinks will only add unique keywords to the note.
func (note *Note) AddLinks(links ...string) {
	for _, link := range links {
		if !containsString(note.Links, link) {
			note.Links = append(note.Links, link)
		}
	}
}

// FromJson unmarshals a buffer.
func (note *Note) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, note); err != nil {
		return err
	}
	return nil
}

// ToJson marshals a buffer.
func (note *Note) ToJson() ([]byte, error) {
	return json.Marshal(note)
}

// contains returns a boolean value indicating if the keyword is contained in the array.
func contains(keywords []Keyword, keyword Keyword) bool {
	lc := strings.ToLower(keyword.Text)
	for _, kw := range keywords {
		if strings.ToLower(kw.Text) == lc {
			return true
		}
	}
	return false
}

// contains returns a boolean value indicating if the link is contained in the array.
func containsString(links []string, link string) bool {
	lc := strings.ToLower(link)
	for _, l := range links {
		if strings.ToLower(l) == lc {
			return true
		}
	}
	return false
}
