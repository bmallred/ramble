package main

import (
	"fmt"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/bmallred/ramble"
	"gitlab.com/bmallred/ramble/database"
)

var (
	// Flags
	depth int
)

var queryCommand = &cobra.Command{
	Use:   "query",
	Short: "Query lists notes based on keywords",
	Long:  "Query lists notes based on keywords",
	Run: func(cmd *cobra.Command, args []string) {
		query(args)
	},
}

func query(filters []string) {
	if len(filters) == 0 {
		fmt.Println("No keywords provided")
		return
	}
	db := &database.Badger{}
	db.Init(dbPath)

	directHits := []*ramble.Note{}
	matchedKeywords := map[string][]string{}
	matchedLinks := map[string][]string{}

	// build a graph from the notes and filter by keywords
	notes, _ := db.Notes()
	graph := ramble.NewGraph(notes)
	found := graph.FindKeyword(nil, filters, depth)

	// gather the direct hits
	for _, note := range found {
		for _, word := range note.Keywords {
			if containsString(word.Text, filters) {
				directHits = append(directHits, note)
				matchedKeywords[note.Path] = append(matchedKeywords[note.Path], "["+Green+word.Text+Reset+"]")
			}
		}

		for _, link := range note.Links {
			if containsString(link, filters) {
				directHits = append(directHits, note)
				matchedLinks[note.Path] = append(matchedLinks[note.Path], "["+Green+link+Reset+"]")
			}
		}
	}

	// find similarities between the initial hit and other notes
	directHits = dedupNotes(directHits)
	for _, note := range found {
		for _, inner := range directHits {
			if note.Path == inner.Path {
				continue
			}

			similar := matchingWords(note, inner)
			for _, s := range similar {
				matchedKeywords[inner.Path] = append(matchedKeywords[inner.Path], "["+Purple+s+Reset+"]")
				matchedKeywords[note.Path] = append(matchedKeywords[note.Path], "["+Purple+s+Reset+"]")
			}

			similar = matchingLinks(note, inner)
			for _, s := range similar {
				matchedLinks[inner.Path] = append(matchedLinks[inner.Path], "["+Purple+s+Reset+"]")
				matchedLinks[note.Path] = append(matchedLinks[note.Path], "["+Purple+s+Reset+"]")
			}
		}
	}

	// Get all file paths for notes and sort them
	keys := []string{}
	for k, _ := range matchedKeywords {
		keys = append(keys, k)
	}
	for k, _ := range matchedLinks {
		keys = append(keys, k)
	}
	keys = dedupStrings(keys)
	sort.Strings(keys)

	// Iterate over all notes outputting the findings
	for _, key := range keys {
		flatKeywords := dedupStrings(matchedKeywords[key])
		flatLinks := dedupStrings(matchedLinks[key])
		sort.Strings(flatKeywords)

		// output filename
		fmt.Printf(White+"%s"+Reset+"\n", key)

		// output keywords
		if _, ok := matchedKeywords[key]; ok {
			fmt.Printf("Keywords: %s\n", strings.Join(flatKeywords, " "))
		}

		// output links
		if _, ok := matchedLinks[key]; ok {
			fmt.Printf("Links: %s\n", strings.Join(flatLinks, " "))
		}

		// skip a line
		fmt.Println()
	}
}

func containsString(word string, words []string) bool {
	for _, w := range words {
		if strings.EqualFold(word, w) {
			return true
		}
	}
	return false
}

func dedupStrings(slice []string) []string {
	keys := make(map[string]bool)
	list := []string{}

	for _, entry := range slice {
		norm := strings.ToLower(entry)
		if _, ok := keys[norm]; !ok {
			keys[norm] = true
			list = append(list, entry)
		}
	}
	return list
}

func matchingWords(a, b *ramble.Note) []string {
	arr := []string{}
	if a.Path == b.Path {
		return arr
	}

	for _, k1 := range a.Keywords {
		for _, k2 := range b.Keywords {
			if strings.EqualFold(k1.Text, k2.Text) {
				arr = append(arr, k1.Text)
			}
		}
	}
	return dedupStrings(arr)
}

func matchingLinks(a, b *ramble.Note) []string {
	arr := []string{}
	if a.Path == b.Path {
		return arr
	}

	for _, k1 := range a.Links {
		for _, k2 := range b.Links {
			if strings.EqualFold(k1, k2) {
				arr = append(arr, k1)
			}
		}
	}
	return dedupStrings(arr)
}

func dedupNotes(slice []*ramble.Note) []*ramble.Note {
	keys := make(map[string]bool)
	list := []*ramble.Note{}

	for _, entry := range slice {
		norm := strings.ToLower(entry.Path)
		if _, ok := keys[norm]; !ok {
			keys[norm] = true
			list = append(list, entry)
		}
	}
	return list
}
