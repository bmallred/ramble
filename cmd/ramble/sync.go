package main

import (
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/bmallred/ramble"
	"gitlab.com/bmallred/ramble/database"
)

var syncCommand = &cobra.Command{
	Use:   "sync",
	Short: "Synchronize notes",
	Long:  "Synchronize notes found in the database and the file system",
	Run: func(cmd *cobra.Command, args []string) {
		sync()
	},
}

func sync() {
	db := &database.Badger{}
	db.Init(dbPath)

	notes, _ := db.Notes()
	for _, existing := range notes {
		// Check if the note still exists
		if !fileExists(existing.Path) {
			// If not then delete the note from storage
			db.Delete(existing)
			continue
		}

		if !existing.IsDirty() {
			// If the note is clean then skip
			continue
		}
		p, err := getParser(existing.Path, AvailableParsers...)
		if err != nil {
			// If there is no parser available then skip it
			continue
		}

		n, err := p.Parse(existing.Path)
		if err != nil {
			// If there was an error parsing then skip
			continue
		}

		existing.Keywords = n.Keywords
		existing.Links = n.Links
		existing.Checksum = n.Checksum
		db.Update(existing)
	}

	filePaths := directoryFiles(".", []string{".md", ".org"})
	for _, fp := range filePaths {
		// Check with the database to see if it already exists
		existing := db.FindByFile(fp)
		if existing != nil {
			// If it already exists then it should have been handled
			continue
		}

		p, err := getParser(fp, AvailableParsers...)
		if err != nil {
			// If there is no parser available then skip it
			continue
		}

		note, err := p.Parse(fp)
		if note == nil || err != nil {
			// If there was an error parsing then skip
			continue
		}

		db.Add(note)
	}

	notes, _ = db.Notes()
	fmt.Printf("Synchronized %d notes\n", len(notes))
}

// GetParser helps find the appropiate parser for a file
func getParser(filePath string, parsers ...ramble.Parser) (ramble.Parser, error) {
	if !fileExists(filePath) {
		return nil, errors.New("File does not exist")
	}

	// Loop through the parsers doing a quick sanity check
	// to see if they can handle the file type. Take the first
	// supported parser found.
	for _, p := range parsers {
		if p.Supports(filePath) {
			return p, nil
		}
	}

	return nil, errors.New("File not supported by available parsers")
}

// Check to see if the file exists and is accessible
func fileExists(fileName string) bool {
	if _, err := os.Stat(fileName); err != nil {
		return false
	}

	return true
}

func directoryFiles(directory string, extensions []string) []string {
	var files []string
	filepath.Walk(directory, func(p string, f os.FileInfo, e error) error {
		if e != nil {
			return e
		}

		if f.IsDir() {
			if strings.HasPrefix(p, ".") {
				return nil
			}
			files = append(files, directoryFiles(path.Join(directory, p), extensions)...)
			return nil
		}

		for _, ext := range extensions {
			if filepath.Ext(p) == ext {
				files = append(files, path.Join(directory, f.Name()))
			}
		}
		return nil
	})
	return files
}
