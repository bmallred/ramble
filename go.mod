module gitlab.com/bmallred/ramble

go 1.15

require (
	github.com/dgraph-io/badger/v3 v3.2011.1
	github.com/jdkato/prose/v2 v2.0.0
	github.com/mvdan/xurls v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/yuin/goldmark v1.3.3
	github.com/yuin/goldmark-meta v1.0.0
)
