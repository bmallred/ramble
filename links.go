package ramble

import (
	"github.com/mvdan/xurls"
)

// ExtractLinks uses `xurls` to parse the content for any potential links.
func ExtractLinks(content []byte) []string {
	return xurls.Relaxed.FindAllString(string(content), -1)
}
