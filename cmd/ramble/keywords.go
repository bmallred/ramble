package main

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/bmallred/ramble"
	"gitlab.com/bmallred/ramble/database"
)

var keywordsCommand = &cobra.Command{
	Use:   "keywords",
	Short: "List keywords",
	Long:  "List all keywords found in the database",
	Run: func(cmd *cobra.Command, args []string) {
		listKeywords(args)
	},
}

func listKeywords(filePaths []string) {
	db := &database.Badger{}
	db.Init(dbPath)

	fpCount := len(filePaths)
	var words []ramble.Keyword
	notes, _ := db.Notes()
	for _, note := range notes {
		if fpCount == 0 {
			words = append(words, note.Keywords...)
			continue
		}

		for _, filePath := range filePaths {
			if strings.HasSuffix(note.Path, filePath) {
				words = append(words, note.Keywords...)
			}
		}
	}

	for _, word := range dedupKeywords(words) {
		fmt.Printf("%s\t%s\n", word.Label, word.Text)
	}
}

func dedupKeywords(slice []ramble.Keyword) []ramble.Keyword {
	keys := make(map[string]bool)
	list := []ramble.Keyword{}

	for _, entry := range slice {
		norm := fmt.Sprintf("%s-%s", entry.Label, entry.Text)
		if _, ok := keys[norm]; !ok {
			keys[norm] = true
			list = append(list, entry)
		}
	}
	return list
}
