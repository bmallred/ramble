package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/bmallred/ramble/database"
)

var notesCommand = &cobra.Command{
	Use:   "notes",
	Short: "List notes",
	Long:  "List all notes found in the database",
	Run: func(cmd *cobra.Command, args []string) {
		listNotes()
	},
}

func listNotes() {
	db := &database.Badger{}
	db.Init(dbPath)

	notes, _ := db.Notes()
	for _, note := range notes {
		fmt.Printf("%s\n", note.Path)
	}
}
