package main

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/bmallred/ramble/database"
)

var forgetCommand = &cobra.Command{
	Use:   "forget",
	Short: "Forget note(s)",
	Long:  "Forget note(s) from the database",
	Run: func(cmd *cobra.Command, args []string) {
		forget(args)
	},
}

func forget(filePaths []string) {
	if len(filePaths) == 0 {
		fmt.Println("No files specified")
		return
	}

	db := &database.Badger{}
	db.Init(dbPath)

	notes, _ := db.Notes()
	for _, note := range notes {
		for _, filePath := range filePaths {
			if strings.HasSuffix(note.Path, filePath) {
				if err := db.Delete(note); err != nil {
					fmt.Printf("Failed to remove %s: %s\n", note.Path, err)
					continue
				}
				fmt.Printf("Removed %s\n", note.Path)
			}
		}
	}
}
