package main

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/bmallred/ramble/database"
)

var linksCommand = &cobra.Command{
	Use:   "links",
	Short: "List links",
	Long:  "List all links found in the database",
	Run: func(cmd *cobra.Command, args []string) {
		listLinks(args)
	},
}

func listLinks(filePaths []string) {
	db := &database.Badger{}
	db.Init(dbPath)

	fpCount := len(filePaths)
	var links []string
	notes, _ := db.Notes()
	for _, note := range notes {
		if fpCount == 0 {
			links = append(links, note.Links...)
			continue
		}

		for _, filePath := range filePaths {
			if strings.HasSuffix(note.Path, filePath) {
				links = append(links, note.Links...)
			}
		}
	}

	for _, link := range dedupStrings(links) {
		fmt.Printf("%s\n", link)
	}
}

func dedupString(slice []string) []string {
	keys := make(map[string]bool)
	list := []string{}

	for _, entry := range slice {
		norm := strings.ToLower(entry)
		if _, ok := keys[norm]; !ok {
			keys[norm] = true
			list = append(list, entry)
		}
	}
	return list
}
