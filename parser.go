package ramble

// Parser is a stream for parsing work unit files
type Parser interface {
	Parse(filePath string) (*Note, error)
	Supports(filePath string) bool
}
