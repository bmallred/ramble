package ramble

type Storage interface {
	Init(dbFile string)
	Notes() ([]*Note, error)
	FindByFile(filePath string) *Note
	Add(note *Note) error
	Delete(note *Note) error
	Update(note *Note) error
}
