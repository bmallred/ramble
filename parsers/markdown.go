package parsers

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/yuin/goldmark"
	"gitlab.com/bmallred/ramble"

	// "github.com/yuin/goldmark/extension"

	meta "github.com/yuin/goldmark-meta"
	"github.com/yuin/goldmark/parser"
)

var (
	rxMarkdownLink = regexp.MustCompile(`\[(.*?)\]\((.*?)\)`)
)

// Markdown parser
type Markdown struct{}

// Supports determines if a file is supported by the parser
func (p *Markdown) Supports(filePath string) bool {
	extensions := []string{".md", ".markdown"}
	for _, ext := range extensions {
		if strings.HasSuffix(strings.ToLower(filePath), ext) {
			return true
		}
	}
	return false
}

// Parse the stream for markdown content
func (p *Markdown) Parse(filePath string) (*ramble.Note, error) {
	note := &ramble.Note{}

	// Open the file for reading
	file, err := os.Open(filePath)
	if err != nil {
		return note, err
	}
	defer file.Close()

	// Assign the path
	note.Path, err = filepath.Abs(filePath)
	if err != nil {
		note.Path, err = filepath.Abs("./" + filePath)
		if err != nil {
			note.Path = filePath
		}
	}

	// Assign the checksum
	note.Checksum, _ = ramble.Checksum(file)
	file.Seek(0, 0)

	// Read the file contents to a byte array
	contents, err := ioutil.ReadAll(file)
	if err != nil {
		return note, err
	}

	// Check for meta-information found in a YAML header
	markdown := goldmark.New(goldmark.WithExtensions(meta.Meta))
	context := parser.NewContext()
	var buffer bytes.Buffer
	if err := markdown.Convert(contents, &buffer, parser.WithContext(context)); err == nil {
		// Recognized keys
		//  - id
		//  - title
		//  - links
		//  - tags
		//  - keywords
		metadata := meta.Get(context)
		for k, v := range metadata {
			switch strings.ToLower(k) {
			case "id":
				note.Id = v.(string)
			case "links":
				note.AddLinks(v.([]string)...)
			case "tags":
				for _, p := range v.([]interface{}) {
					note.AddKeywords(ramble.Keyword{
						Label: "TAG",
						Text:  p.(string),
					})
				}
			case "keywords":
				for _, p := range v.([]interface{}) {
					note.AddKeywords(ramble.Keyword{
						Label: "YAML",
						Text:  p.(string),
					})
				}
			}
		}
	}

	// Assign the keywords from entity extraction
	note.AddKeywords(ramble.ExtractKeywords(contents)...)

	// Assign any links
	note.AddLinks(ramble.ExtractLinks(contents)...)
	if rxMarkdownLink.Match(contents) {
		for _, match := range rxMarkdownLink.FindAllSubmatch(contents, -1) {
			if len(match) < 3 {
				continue
			}
			note.AddLinks(string(match[2]))
		}
	}

	// Assign an identifier if one has not been set yet
	if note.Id == "" {
		info, err := file.Stat()
		if err == nil {
			note.Id = info.ModTime().Format(time.RFC3339) + "_" + info.Name()
		}
	}

	return note, nil
}
