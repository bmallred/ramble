package main

import (
	"runtime"

	"github.com/spf13/cobra"
)

var (
	// Flags
	dbPath  string
	noColor bool

	// Terminal colors
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
	Blue   = "\033[34m"
	Purple = "\033[35m"
	Cyan   = "\033[36m"
	Gray   = "\033[37m"
	White  = "\033[97m"
)

var rootCommand = &cobra.Command{
	Use:   "ramble",
	Short: "",
	Long:  "",
}

func Execute() error {
	if noColor {
		nullifyColor()
	}
	return rootCommand.Execute()
}

func init() {
	rootCommand.Flags().BoolVarP(&noColor, "no-color", "c", false, "Turn off console colors")
	rootCommand.Flags().StringVarP(&dbPath, "db", "", ".ramble", "Path to database file")

	queryCommand.Flags().IntVarP(&depth, "depth", "d", 0, "Depth of related links")

	rootCommand.AddCommand(syncCommand, keywordsCommand, linksCommand, notesCommand, queryCommand, forgetCommand)

	// Terminal colors
	if runtime.GOOS == "windows" {
		nullifyColor()
	}
}

func nullifyColor() {
	Reset = ""
	Red = ""
	Green = ""
	Yellow = ""
	Blue = ""
	Purple = ""
	Cyan = ""
	Gray = ""
	White = ""
}
