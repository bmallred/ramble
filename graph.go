package ramble

import (
	"strings"
)

type Graph struct {
	Nodes []*Node
	Edges []*Edge
}

type Node struct {
	Note *Note
}

type Edge struct {
	A, B *Note
}

func NewGraph(notes []*Note) *Graph {
	graph := &Graph{}

	for _, n1 := range notes {
		graph.Nodes = append(graph.Nodes, &Node{Note: n1})

		for _, n2 := range notes {
			if n1.Path == n2.Path {
				continue
			}

			// Build an edge if they contain similarities in keywords
			if containsSimilarKeyword(n1.Keywords, n2.Keywords) && !graph.hasEdge(n1, n2) {
				graph.Edges = append(graph.Edges, &Edge{A: n1, B: n2})
			}

			// Build an edge if they contain similarities in links
			if containsSimilarLinks(n1.Links, n2.Links) && !graph.hasEdge(n1, n2) {
				graph.Edges = append(graph.Edges, &Edge{A: n1, B: n2})
			}
		}
	}

	return graph
}

func containsSimilarKeyword(awords, bwords []Keyword) bool {
	for _, aword := range awords {
		for _, bword := range bwords {
			if strings.EqualFold(aword.Text, bword.Text) {
				return true
			}
		}
	}
	return false
}

func containsSimilarLinks(awords, bwords []string) bool {
	for _, aword := range awords {
		for _, bword := range bwords {
			if strings.EqualFold(aword, bword) {
				return true
			}
		}
	}
	return false
}

func (graph *Graph) hasEdge(a, b *Note) bool {
	for _, edge := range graph.Edges {
		if (edge.A.Path == a.Path && edge.B.Path == b.Path) || (edge.B.Path == a.Path && edge.A.Path == b.Path) {
			return true
		}
	}
	return false
}

func (graph *Graph) AddNode(nodes ...*Node) {
	graph.Nodes = append(graph.Nodes, nodes...)
}

func (graph *Graph) AddEdge(edges ...*Edge) {
	graph.Edges = append(graph.Edges, edges...)
}

func (graph *Graph) FindKeyword(start *Note, words []string, depth int) []*Note {
	matches := []*Note{}

	if start != nil {
		matches = append(matches, start)
	}

	// If start is nil then we need to search all nodes
	if start == nil {
		for _, node := range graph.Nodes {
			if containsEveryKeyword(node.Note, words) {
				matches = append(matches, node.Note)
			}
		}
	}

	if depth == 0 {
		return matches
	}

	matchesDeep := []*Note{}
	for _, match := range matches {
		// Find matches to the next depth
		for _, edge := range graph.Edges {
			if edge.A.Path == match.Path {
				deep := graph.FindKeyword(edge.B, words, depth-1)
				matchesDeep = append(matchesDeep, deep...)
			} else if edge.B.Path == match.Path {
				deep := graph.FindKeyword(edge.A, words, depth-1)
				matchesDeep = append(matchesDeep, deep...)
			}
		}
	}

	matches = append(matches, matchesDeep...)
	return dedupNotes(matches)
}

func containsEveryKeyword(note *Note, words []string) bool {
	count := 0
	for _, word := range words {
		for _, kw := range note.Keywords {
			if strings.EqualFold(kw.Text, word) {
				count += 1
			}
		}
	}
	return count == len(words)
}

func dedupNotes(slice []*Note) []*Note {
	keys := make(map[string]bool)
	list := []*Note{}

	for _, entry := range slice {
		norm := strings.ToLower(entry.Path)
		if _, ok := keys[norm]; !ok {
			keys[norm] = true
			list = append(list, entry)
		}
	}
	return list
}
