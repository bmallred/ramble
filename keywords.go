package ramble

import (
	"regexp"
	"strings"

	prose "github.com/jdkato/prose/v2"
)

var (
	blacklist     = regexp.MustCompile(`[^0-9A-Za-z]`)
	minimumLength = 5
)

// ExtractKeywords will extract entities from the body of text.
func ExtractKeywords(contents []byte) []Keyword {
	var keywords []Keyword
	doc, err := prose.NewDocument(string(contents), prose.WithSegmentation(false))
	if err != nil {
		return keywords
	}

	// For use with tokens
	for _, tok := range doc.Tokens() {
		// log.Printf("%s\t%s\t%s", tok.Label, tok.Tag, tok.Text)
		text := strings.TrimSpace(blacklist.ReplaceAllString(tok.Text, " "))
		if len(text) < minimumLength {
			continue
		}
		if strings.Contains(text, " ") {
			continue
		}

		switch tok.Tag {
		case "NN":
			fallthrough
		case "JJ":
			fallthrough
		case "VB":
			keywords = append(keywords, Keyword{
				Label: tok.Tag,
				Text:  text,
			})
		}
	}

	// // For use with entities
	// for _, entity := range doc.Entities() {
	// 	keywords = append(keywords, Keyword{
	// 		Label: entity.Label,
	// 		Text:  entity.Text,
	// 	})
	// }

	return keywords
}
