package main

import (
	"gitlab.com/bmallred/ramble"
	"gitlab.com/bmallred/ramble/parsers"
)

// AvailableParsers is an array of default parsers within the package
var AvailableParsers = []ramble.Parser{
	&parsers.Markdown{},
	&parsers.Orgmode{},
}

func main() {
	Execute()
}
