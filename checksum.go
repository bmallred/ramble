package ramble

import (
	"crypto/sha256"
	"fmt"
	"io"
)

// Checksum returns the checksum hash of the contents in the io.Reader.
func Checksum(reader io.Reader) (string, error) {
	hash := sha256.New()
	if _, err := io.Copy(hash, reader); err != nil {
		return "", err
	}

	sum := hash.Sum(nil)
	return fmt.Sprintf("%x", sum), nil
}
