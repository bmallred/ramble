package database

import (
	"log"

	badger "github.com/dgraph-io/badger/v3"
	"gitlab.com/bmallred/ramble"
)

// Badger stores our currently knowledgebase.
type Badger struct {
	Path string
}

// options used in the badger database.
func (storage *Badger) options() badger.Options {
	options := badger.DefaultOptions(storage.Path)
	options.Logger = nil
	return options
}

// Init will setup the database for use and make sure we have the appropriate access.
func (storage *Badger) Init(dbFile string) {
	storage.Path = dbFile

	// Sanity check
	db, err := badger.Open(storage.options())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
}

// Notes returns all notes stored in the database.
func (storage *Badger) Notes() ([]*ramble.Note, error) {
	db, err := badger.Open(storage.options())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var noteValues [][]byte
	err = db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			err := item.Value(func(v []byte) error {
				var buffer []byte
				buffer = append(buffer, v...)
				noteValues = append(noteValues, buffer)
				return nil
			})

			if err != nil {
				return err
			}
		}

		return nil
	})

	if err != nil {
		// log.Printf("Failed to collect notes: %v", err)
		return nil, err
	}

	var notes []*ramble.Note
	for _, barr := range noteValues {
		note := &ramble.Note{}
		err := note.FromJson(barr)
		if err != nil {
			return nil, err
		}
		notes = append(notes, note)
	}

	return notes, nil
}

// FindByFile will return the note found with the file path. Null if not found.
func (storage *Badger) FindByFile(filePath string) *ramble.Note {
	db, err := badger.Open(storage.options())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var noteValue []byte
	err = db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(filePath))
		if err != nil {
			return err
		}

		return item.Value(func(v []byte) error {
			noteValue = append([]byte{}, v...)
			return nil
		})
	})

	if err != nil {
		// log.Printf("Failed to find note by file [%s]: %v", filePath, err)
		return nil
	}

	note := &ramble.Note{}
	err = note.FromJson(noteValue)
	if err != nil {
		// log.Printf("Failed to read note [%s]: %v", filePath, err)
		return nil
	}

	return note
}

// Add stores the note in the database.
func (storage *Badger) Add(note *ramble.Note) error {
	db, err := badger.Open(storage.options())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	return db.Update(func(txn *badger.Txn) error {
		noteValue, _ := note.ToJson()
		e := badger.NewEntry([]byte(note.Path), noteValue)
		return txn.SetEntry(e)
	})
}

// Delete removes the note from the database.
func (storage *Badger) Delete(note *ramble.Note) error {
	db, err := badger.Open(storage.options())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	return db.Update(func(txn *badger.Txn) error {
		return txn.Delete([]byte(note.Path))
	})
}

// Update store the note in the database.
func (storage *Badger) Update(note *ramble.Note) error {
	db, err := badger.Open(storage.options())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	return db.Update(func(txn *badger.Txn) error {
		buffer, _ := note.ToJson()
		return txn.Set([]byte(note.Path), buffer)
	})
}
