package parsers

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gitlab.com/bmallred/ramble"
)

var (
	rxOrgmodeLink = regexp.MustCompile(`\[\[(.*?)\]`)
	rxOrgmodeTag  = regexp.MustCompile(`\:(.*?)\:`)
)

// Orgmode parser
type Orgmode struct{}

// Supports determines if a file is supported by the parser
func (parser *Orgmode) Supports(filePath string) bool {
	extensions := []string{".org"}
	for _, ext := range extensions {
		if strings.HasSuffix(strings.ToLower(filePath), ext) {
			return true
		}
	}
	return false
}

// Parse the stream for markdown content
func (parser *Orgmode) Parse(filePath string) (*ramble.Note, error) {
	note := &ramble.Note{}

	// Open the file for reading
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// Assign the path
	note.Path, err = filepath.Abs(filePath)
	if err != nil {
		note.Path, err = filepath.Abs("./" + filePath)
		if err != nil {
			note.Path = filePath
		}
	}

	// Assign the checksum
	note.Checksum, _ = ramble.Checksum(file)
	file.Seek(0, 0)

	// Read the file contents to a byte array
	contents, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	// Check for meta-information
	// #+FILETAGS: :tag1:tag2:tag3:
	if rxOrgmodeTag.Match(contents) {
		for _, match := range rxOrgmodeTag.FindAllSubmatch(contents, -1) {
			if len(match) < 2 {
				continue
			}

			note.AddKeywords(ramble.Keyword{
				Label: "TAG",
				Text:  string(match[1]),
			})
		}
	}

	// Assign the keywords from entity extraction
	note.AddKeywords(ramble.ExtractKeywords(contents)...)

	// Assign any links
	note.AddLinks(ramble.ExtractLinks(contents)...)
	if rxOrgmodeLink.Match(contents) {
		for _, match := range rxOrgmodeLink.FindAllSubmatch(contents, -1) {
			if len(match) < 2 {
				continue
			}
			note.AddLinks(string(match[1]))
		}
	}

	// Assign an identifier if one has not been set yet
	if note.Id == "" {
		info, err := file.Stat()
		if err == nil {
			note.Id = info.ModTime().Format(time.RFC3339) + "_" + info.Name()
		}
	}

	return note, nil
}
